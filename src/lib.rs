#[macro_use]
extern crate vst;

use vst::{buffer::AudioBuffer, plugin::{PluginParameters, HostCallback, Info, Plugin}};
use std::sync::Arc;

pub mod parameter;
use parameter::ParameterObject;


fn sinus(phase: f64) -> f64{
    (phase * (2.0 * std::f64::consts::PI)).sin()
}

fn saw(phase: f64) -> f64{
    (phase.fract() * 2.0) - 1.0
}

fn sq(phase: f64) -> f64{
    if phase.fract() < 0.5{
        1.0
    }else{
        -1.0
    }
}

fn tri(phase: f64) -> f64{
    let phase = phase.fract();
    if phase < 0.25{
        //going down from 0.0
        -(phase * 4.0)
    }else if phase < 0.75{
        -1.0 + ((phase - 0.25) * 4.0)
    }else{
        1.0 - (phase - 0.75) * 4.0
    }
}


#[derive(Default)]
struct BasicPlugin{
    parameter: Arc<ParameterObject>,
    phase: f64,
    rate: f32,
    block_size: i64,
    freq: f64,
}

impl BasicPlugin{
    fn timer_per_block(&self, num_sample: usize) -> f64{
        (1.0 as f64 / self.rate as f64) * num_sample as f64
    }

    fn next_sample(&mut self, phase: f64) -> f64{
        let phase = phase * (self.freq + (500.0 * (*self.parameter.detune.lock().unwrap() - 0.5) as f64));
        let wf = *self.parameter.waveform.lock().unwrap();        
        let sample = if wf < 0.25{
            sinus(phase)
        }else if wf < 0.5{
            saw(phase)
        }else if wf < 0.75{
            tri(phase)
        }else{
            sq(phase)
        };
        
        sample
    }
}


impl Plugin for BasicPlugin {
    fn new(_host: HostCallback) -> Self {
        BasicPlugin{
            phase: 0.0,
            rate: 44100.0,
            block_size: 256,
            freq: 2616.25 / 2.0,
            parameter: Arc::new(ParameterObject::default())
        }
    }

    fn get_info(&self) -> Info {
        Info {
            name: "Basic Plugin".to_string(),
            unique_id: 1357, // Used by hosts to differentiate between plugins.
            parameters: ParameterObject::num_parameters(),
            ..Default::default()
        }
    }

    fn set_sample_rate(&mut self, rate: f32){
        self.rate = rate;
        println!("New samplerate: {}", self.rate);
    }

    fn set_block_size(&mut self, size: i64){
        self.block_size = size;
        //println!("New blocksize: {}", self.block_size);
    }

    fn process(&mut self, buffer: &mut AudioBuffer<'_, f32>){

        let time_per_sample = self.timer_per_block(1);
        let samples = buffer.samples();
        // For each input and output
        for (input, output) in buffer.zip() {
            
            let mut iphase = self.phase;

            let mut tmpbuffer = Vec::with_capacity(samples);
            // For each input sample and output sample in buffer
            for (_in_sample, _out_sample) in input.into_iter().zip(output.into_iter()) {
                tmpbuffer.push(self.next_sample(iphase) as f32);
                iphase += time_per_sample;
            }

            //Filter buffer by changing into i16 space, applying the filer and changing back
            let mut i32filter = tmpbuffer.into_iter().map(|f| (f * i32::MAX as f32) as i32).collect::<Vec<_>>();
            lowpass_filter::simple::dp::apply_lpf_i32_dp(&mut i32filter, self.rate as u16, self.parameter.get_cutoff_freq());

            //Convert back into f32 space
            let fbuffer = i32filter.into_iter().map(|v| v as f32 / i32::MAX as f32).collect::<Vec<_>>();
            
            // For each input sample and output sample in buffer
            for (idx, (_in_sample, out_sample)) in input.into_iter().zip(output.into_iter()).enumerate() {
                *out_sample = fbuffer[idx];
                iphase += time_per_sample;
            }
        }

        self.phase += self.timer_per_block(buffer.samples());
    }

    fn get_parameter_object(&mut self) -> Arc<dyn PluginParameters>{
        self.parameter.clone()
    }
}


plugin_main!(BasicPlugin); // Important!



