use std::sync::Mutex;

use vst::plugin::PluginParameters;


pub struct ParameterObject{
    pub waveform: Mutex<f32>,
    pub cutoff: Mutex<f32>,
    pub detune: Mutex<f32>,
}

impl ParameterObject{
    pub fn num_parameters() -> i32{
        3
    }

    pub fn get_cutoff_freq(&self) -> u16{
        (((*self.cutoff.lock().unwrap()).powf(4.0).max(1.0 / 10000.0) * 22050.0)) as u16
    }
}

impl Default for ParameterObject{
    fn default() -> Self{
        ParameterObject{
            waveform: Mutex::new(0.0),
            cutoff: Mutex::new(0.1),
            detune: Mutex::new(0.5),
        }
    }
}


impl PluginParameters for ParameterObject{
    fn get_parameter_label(&self, index: i32) -> String {
        match index{
            0 => "waveform".to_string(),
            1 => "cutoff".to_string(),
            2 => "detune".to_string(),
            _ => "none".to_string()
        }
    }

    fn get_parameter_name(&self, index: i32) -> String {
        self.get_parameter_label(index)
    }

    fn get_parameter(&self, index: i32) -> f32 {
        match index{
            0 => *self.waveform.lock().unwrap(),
            1 => *self.cutoff.lock().unwrap(),
            2 => *self.detune.lock().unwrap(),
            _ => {
                println!("Warning tried to get parameter out of range");
                0.0
            }
        }
    }

    fn set_parameter(&self, index: i32, value: f32) {
        match index{
            0 => *self.waveform.lock().unwrap() = value,
            1 => *self.cutoff.lock().unwrap() = value,
            2 => *self.detune.lock().unwrap() = value,
            _ => {}
        }
    }

    fn can_be_automated(&self, index: i32) -> bool {
        if index < 100{
            true
        }else{
            false
        }
    }
}
